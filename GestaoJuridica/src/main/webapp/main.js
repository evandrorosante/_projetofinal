var contAdv = 0;
var contSec = 0;
function tipoUsuario(local) {
    var tipo = document.getElementById('tipo').value;

    if (tipo == "advogado" && contAdv == 0) {
        contAdv++;
        contSec = 0;

        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'op';
        input.value = 'cadastraAdvogado';
        input.id = 'cadastraAdvogado';
        document.getElementById(local).appendChild(input);

        var label = document.createElement('label');
        label.innerHTML = '<input id="nrooab" onblur="validarOAB()" data-mask="999999" required type="text" name="oab" placeholder="OAB"/>\n\
                <div id="divMsgOAB">';
        label.id = 'labeloab';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<select name="uf" id="estado"> <option value="">Selecione um Estado</option>\n\
            <option value="AC">Acre</option><option value="AL">Alagoas</option><option value="AM">Amazonas</option>\n\
            <option value="AP">Amapá</option><option value="BA">Bahia</option><option value="CE">Ceará</option>\n\
            <option value="DF">Distrito Federal</option><option value="ES">Espírito Santo</option><option value="GO">Goiás</option>\n\
            <option value="MA">Maranhão</option><option value="MG">Minas Gerais</option><option value="MS">Mato Grosso do Sul</option>\n\
            <option value="MT">Mato Grosso</option><option value="PA">Pará</option><option value="PB">Paraíba</option>\n\
            <option value="PE">Pernambuco</option><option value="PI">Piauí</option><option value="PR">Paraná</option>\n\
            <option value="RJ">Rio de Janeiro</option><option value="RN">Rio Grande do Norte</option><option value="RO">Rondônia</option>\n\
            <option value="RR">Roraima</option><option value="RS">Rio Grande do Sul</option><option value="SC">Santa Catarina</option>\n\
            <option value="SP">São Paulo</option><option value="SE">Sergipe</option><option value="TO">Tocantins</option></select>';
        label.id = 'labelEstado';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<input id="advinputcpf" onblur="validarCPF()" required type="text"\n\
            name="cpf" data-mask="999.999.999-99" placeholder="CPF"/><div id="divMsgCPF"></div>';
        label.id = 'advlabelcpf';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<input id="advinputnome" required type="text" name="nome" placeholder="Nome"/>';
        label.id = 'advlabelnome';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<input id="advinputemail" required onblur="validarEmail()" type="email" name="email" \n\
            placeholder="E-mail"/><div id="divMsgEmail"></div>';
        label.id = 'advlabelemail';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<button disabled id="botao" class="btn-primary text-center"><i class="icon-ok-sign"></i>Salvar</button><button type="button" id="botao2" class="btn-info" Onclick="window.history.go(-1)"> <i class="icon-backward"></i> Voltar</button>';
        label.id = 'advlabelbotao';
        label.className = 'text-center';
        document.getElementById(local).appendChild(label);

        document.getElementById('cadastraSecretaria').remove();
        document.getElementById('secinputcpf').remove();
        document.getElementById('secinputnome').remove();
        document.getElementById('secinputemail').remove();
        document.getElementById('seclabelcpf').remove();
        document.getElementById('seclabelnome').remove();
        document.getElementById('seclabelemail').remove();
        document.getElementById('seclabelbotao').remove();

    }
    if (tipo == "secretaria" && (contSec == 0)) {

        contAdv = 0;
        contSec++;

        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'op';
        input.value = 'cadastraSecretaria';
        input.id = 'cadastraSecretaria';
        document.getElementById(local).appendChild(input);

        var label = document.createElement('label');
        label.innerHTML = '<input id="secinputcpf" required type="text" name="cpf"\n\
            data-mask="999.999.999-99" onblur="validarCPF()" placeholder="CPF"/><div id="divMsgCPF"></div>';
        label.id = 'seclabelcpf';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<input id="secinputnome" required type="text" name="nome" placeholder="Nome"/>';
        label.id = 'seclabelnome';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<input id="secinputemail" onblur="validarEmail()" required type="email" name="email" placeholder="E-mail"/>\n\
            <div id="divMsgEmail"></div>';
        label.id = 'seclabelemail';
        label.className = 'breadcrumb text-center';
        document.getElementById(local).appendChild(label);

        var label = document.createElement('label');
        label.innerHTML = '<button disabled id="botao" class="btn-primary text-center"><i class="icon-ok-sign"></i>Salvar</button><button type="button" id="botao2" class="btn-info" Onclick="window.history.go(-1)"> <i class="icon-backward"></i> Voltar</button>';
        label.id = 'seclabelbotao';
        label.className = 'text-center';
        document.getElementById(local).appendChild(label);

        document.getElementById('cadastraAdvogado').remove();
        document.getElementById('nrooab').remove();
        document.getElementById('estado').remove();
        document.getElementById('advinputcpf').remove();
        document.getElementById('advinputnome').remove();
        document.getElementById('advinputemail').remove();
        document.getElementById('labelEstado').remove();
        document.getElementById('advlabelcpf').remove();
        document.getElementById('advlabelnome').remove();
        document.getElementById('advlabelemail').remove();
        document.getElementById('labeloab').remove();
        document.getElementById('advlabelbotao').remove();
    }
}

function tipoPessoa() {
    var tipo = document.getElementById('tipo').value;

    if (tipo == "fisica") {
        $("#pessoaJuridica").remove();
        $("#fisica").append('<div id="pessoaFisica"><input type="hidden" value="cadastrarClientePF" name="op">\n\
            <h4 class="text-center">Dados Pessoais </h4><label class="breadcrumb text-center">\n\
            <input required onblur="validarCPF()" type="text" name="cpf" data-mask="999.999.999-99" placeholder="CPF"/>\n\
            <div id="divMsgCPF" style="font-size: 10pt"></div></label><label class="breadcrumb text-center"><input required type="text" name="rg" placeholder="RG"/>\n\
            </label><label class="breadcrumb text-center"><input required type="text" name="nome" placeholder="Nome completo"/>\n\
            </label><label class="breadcrumb text-center"> Data de Nascimento<br><input required type="date" name="datanasc" placeholder="Data de nascimento"/>\n\
            </label><label class="breadcrumb text-center"><input required type="text" name="nacionalidade" placeholder="Nacionalidade"/></label>\n\
            <label class="breadcrumb text-center"><select required name="estadoCivil"><option value="">Estado Civil</option>\n\
            <option value="Solteiro">Solteiro</option><option value="Casado">Casado</option>\n\
            <option value="Divorciado">Divorciado</option>\n\
            <option value="Separado Judicialmente">Separado Judicialmente</option></select></label>\n\
             <label class="breadcrumb text-center"><input required type="text" name="profissao" placeholder="Profissao"/></label></div>');
        $("#geral").show();
    }
    if (tipo == "juridica") {
        $("#juridica").append('<div id="pessoaJuridica"><input type="hidden" value="cadastrarClientePJ" name="op">\n\
            <h4 class="text-center">Dados da Empresa</h4><label class="breadcrumb text-center">\n\
            <input required type="text" onblur="validarCNPJ()"  name="cnpj" data-mask="99.999.999/9999-99" placeholder="CNPJ"/>\n\
            <div id="divMsgCNPJ" style="font-size: 10pt"></div></label><label class="breadcrumb text-center"><input maxlength="20" required type="text" name="inscricaoEstadual" placeholder="Inscricao Estadual" />\n\
            </label><label class="breadcrumb text-center"><input type="text" required name="razaoSocial" placeholder="Razao Social" />\n\
            </label><label class="breadcrumb text-center"><input type="text" required name="nomeFantasia" placeholder="Nome Fantasia" />\n\
            </label></div>');
        $("#pessoaFisica").remove();
        $("#geral").show();

    }
    if (tipo == "vazio") {
        $("#pessoaFisica").remove();
        $("#pessoaJuridica").remove();
        $("#geral").hide();
    }
}
function validarEmail() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=email',
        data: 'variavel=' + $('input[name=email]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgEmail").style.color = "blue";
                document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgEmail").style.color = "red";
                document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgEmail").innerHTML = (dados);
        }
    });
}
function validarCPF() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=cpf',
        data: 'variavel=' + $('input[name=cpf]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgCPF").style.color = "blue";
                // document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgCPF").style.color = "red";
                //document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgCPF").innerHTML = (dados);
        }
    });
}

function validarCpfProcesso() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=cpfProcesso',
        data: 'variavel=' + $('input[name=cpf]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgCPF").style.color = "blue";
                // document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgCPF").style.color = "red";
                //document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgCPF").innerHTML = (dados);
        }
    });
}

function validarCNPJ() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=cnpj',
        data: 'variavel=' + $('input[name=cnpj]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgCNPJ").style.color = "blue";
                // document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgCNPJ").style.color = "red";
                //document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgCNPJ").innerHTML = (dados);
        }
    });
}

function validarCnpjProcesso() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=cnpjProcesso',
        data: 'variavel=' + $('input[name=cnpj]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgCNPJ").style.color = "blue";
                // document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgCNPJ").style.color = "red";
                //document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgCNPJ").innerHTML = (dados);
        }
    });
}

function validarOAB() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=oab',
        data: 'variavel=' + $('input[name=oab]').val(),
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            if (dados == "<i class='icon-ok-sign'></i>") {
                document.getElementById("divMsgOAB").style.color = "blue";
                // document.getElementById("botao").disabled = false;
            } else {
                document.getElementById("divMsgOAB").style.color = "red";
                //document.getElementById("botao").disabled = true;
            }
            console.log(dados);
            document.getElementById("divMsgOAB").innerHTML = (dados);
        }
    });
}

function tipoCliente() {
    var tipo = document.getElementById('tipo').value;

    if (tipo == "fisica") {
        $("#pessoaJuridica").remove();
        $("#fisica").append('<div id="pessoaFisica"><input required onblur="validarCpfProcesso()" type="text" name="cpf" data-mask="999.999.999-99" placeholder="CPF"/>\n\
           <input type="hidden" value="cadastraProcessoPF" name="op"><div id="divMsgCPF" style="font-size: 10pt"></div></div>');
        $("#geral").show();
    }
    if (tipo == "juridica") {
        $("#juridica").append('<div id="pessoaJuridica"><input required onblur="validarCnpjProcesso()" type="text" name="cnpj" data-mask="99.999.999/9999-99" placeholder="CNPJ"/>\n\
          <input type="hidden" value="cadastraProcessoPJ" name="op"><div id="divMsgCNPJ" style="font-size: 10pt"></div></div>');
        $("#pessoaFisica").remove();
        $("#geral").show();

    }
    if (tipo == "vazio") {
        $("#pessoaFisica").remove();
        $("#pessoaJuridica").remove();
        $("#geral").hide();
    }
}

function adicionaAdv() {
    $("#advogado").append('<br><select class="escolheAdv" name="oab"></select>');
}

function removeAdv() {
    $("#advogado").empty();
}


function  carregaAdvogados() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=carregaAdvogados',
        // data: 'variavel=teste'/* + $('select[name=advogado]').val()*/,
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            var pegaDados = dados.split(":");

            for (var i = 0; i < pegaDados.length - 1; i++) {
                var nome = pegaDados[i].split("-")[0];
                var oab = pegaDados[i].split("-")[1];
                $("select[class=escolheAdv]").append("<option value='" + oab + "'>" + nome + "</option>");
            }
        }
    });
}

function  carregaAdvogadosEditar() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=carregaAdvogados',
        // data: 'variavel=teste'/* + $('select[name=advogado]').val()*/,
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            var pegaDados = dados.split(":");
            $("select[class=escolheAdv]").append("<option value=''>Selecionar</option>");
            for (var i = 0; i < pegaDados.length - 1; i++) {
                var nome = pegaDados[i].split("-")[0];
                var oab = pegaDados[i].split("-")[1];
                $("select[class=escolheAdv]").append("<option value='" + oab + "'>" + nome + "</option>");
            }
        }
    });
}

function selecionaBusca() {
    var tipo = $("select[name=op]").val();

    if (tipo == "ConsultaPF") {
        $("#pessoa").append("<span id='pf'><select name='coluna'> <option value='nome'>Nome</option>\n\
            <option value='cpf'>CPF</option> </select></span>");
        $("#pj").remove();
    }

    if (tipo == "ConsultaPJ") {
        $("#pessoa").append("<span id='pj'><select name='coluna'> <option value='nome_fantasia'>Nome Fantasia</option>\n\
             <option value='cnpj'>CNPJ</option> </select></span>");
        $("#pf").remove();
    }

    if (tipo == "") {
        $("#pf").remove();
        $("#pj").remove();
    }
}

function editaEstado(estado) {
    $("select[name=estado] option[value='" + estado + "']").attr("selected", true);
}

function  carregaProcessos() {
    $.ajax({
        type: 'GET',
        url: 'ValidacaoClientes?op=carregaProcessos',
        // data: 'variavel=teste'/* + $('select[name=advogado]').val()*/,
        statusCode: {
            404: function () {
                alert('Página não encontrada');
            },
            500: function () {
                alert('Erro no servidor');
            }
        },
        success: function (dados) {
            var pegaDados = dados.split(":");

            for (var i = 0; i < pegaDados.length - 1; i++) {
                var idProcesso = pegaDados[i].split("-")[0];
                var numProcesso = pegaDados[i].split("-")[1];
                if (numProcesso != '0') {
                    $("select[id=processo]").append("<option value='" + idProcesso + "'>" + numProcesso + "</option>");
                    console.log("ID: " + idProcesso + " Numero" + numProcesso);
                }
            }
        }
    });
}

function buscaEndereco() {

    consulta = $("#cep").val();

    var url = "http://cep.correiocontrol.com.br/" + consulta + ".json";

    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        success: function (json) {
            $("input[name=logradouro]").val(json.logradouro);
            $("input[name=bairro]").val(json.bairro);
            $("select[name=cidade]").append("<option value='" + json.localidade + "'>" + json.localidade + "</option>");
            $("select[name=cidade] option[value='" + json.localidade + "']").attr("selected", true);
            $("select[name=estado] option[value='" + json.uf + "']").attr("selected", true);
        }
    });//ajax

}


